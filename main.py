import time, vk, sys
inp    = open("access_token")
token  = inp.read()
session = vk.Session(access_token=token[:85])
api = vk.API(session)


def like   (own, itm):
  api.likes.add   (type="photo", owner_id=own, item_id=itm)
def dislike(own, itm):
  api.likes.delete(type="photo", owner_id=own, item_id=itm)

def __main():
  isLiked = True
  cnt, uid = 0, sys.argv[1]
  photo = api.users.get(user_ids=uid, fields="crop_photo")[0]['crop_photo']['photo']['pid']
  print(photo);
  print("started")
  
  like(uid, photo)
  while 1:
    if(isLiked):
      dislike(uid, photo)
      isLiked = False
      cnt+=1
    else:
      like(uid, photo)
      isLiked = True
      print(cnt)
    time.sleep(1)
  
__main()
